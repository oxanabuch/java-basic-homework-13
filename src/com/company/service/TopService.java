package com.company.service;

import com.company.dto.BuyingDto;
import com.company.dto.ReportDto;
import com.company.dto.StoreDto;
import com.company.dto.TopDto;
import com.company.model.Store;
import com.company.model.product.Buying;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TopService {

    public static final String SUMMARY_REPORT = "Top 5 report";

    private Store[] stores;

    public TopService(Store[] stores) {

        this.stores = stores;
    }

    public TopDto build() {
        StoreDto[] storeDtos = mapStores();
        BuyingDto[] buyingDtos = mergeBuyings(storeDtos);

        //---------------
        BuyingDto[] sortedBuyingDtos = Arrays.stream(buyingDtos)
                .sorted(Comparator.comparingInt(BuyingDto::getCount)
                        .reversed())
                .toList()
                .toArray(new BuyingDto[0]);
        //-------------------

        double totalSum = 0;

        for (BuyingDto buyingDto : sortedBuyingDtos) {
            totalSum += buyingDto.getSum();
        }

        return new TopDto(SUMMARY_REPORT, sortedBuyingDtos, totalSum);
    }

    private BuyingDto[] mergeBuyings(StoreDto[] storeDtos) {

        int count = getCount(storeDtos);

        BuyingDto[] totalArr = new BuyingDto[count];

        int i = 0;

        for (StoreDto storeDto : storeDtos) {

            for (BuyingDto sell : storeDto.getSells()) {
                totalArr[i++] = sell;
            }

        }

        return totalArr;

    }

    private int getCount(StoreDto[] storeDtos) {
        int count = 0;
        for (StoreDto storeDto : storeDtos) {
            count += storeDto.getSells().length;
        }
        return count;
    }

    private StoreDto[] mapStores() {

        StoreDto[] array = new StoreDto[stores.length];

        for (int i = 0; i < array.length; i++) {

            Store store = stores[i];
            String name = store.getName();
            BuyingDto[] buyingDtos = new BuyingDto[store.getSales().length];

            for (int j = 0; j < store.getSales().length; j++) {

                Buying sale = store.getSales()[j];
                double sum = sale.getProduct().getCost() * sale.getCount();
                buyingDtos[j] = new BuyingDto(sale.getProduct().getName(), sale.getCount(), sum);

            }

            array[i] = new StoreDto(name, buyingDtos);

        }

        return array;
    }
}

