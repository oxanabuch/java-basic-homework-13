package com.company.controller;

import com.company.dto.ReportDto;
import com.company.dto.TopDto;
import com.company.model.Store;
import com.company.service.ReportService;
import com.company.service.TopService;
import com.company.view.ReportView;
import com.company.view.TopView;

public class  ReportController {

    public void execute(Store[] stores) {
        ReportService reportService = new ReportService(stores);
        ReportDto report = reportService.build();
        ReportView view = new ReportView();
        view.printDetails(report);
    }

    public void showTop5(Store[] stores) {
        TopService topService = new TopService(stores);
        TopDto report = topService.build();
        TopView view = new TopView();
        view.printDetails(report);
    }
}
