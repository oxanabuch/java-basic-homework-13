package com.company.view;

import com.company.dto.BuyingDto;
import com.company.dto.ReportDto;
import com.company.dto.TopDto;

import java.util.TreeSet;

public class TopView {

    public static final String DESCRIPTION = "REPORT DATA: ";

    public void printDetails(TopDto report) {

        int dash = 50;

        System.out.println("=".repeat(dash));
        System.out.println(DESCRIPTION + report.getName());
        System.out.println("=".repeat(dash));

        System.out.printf("%-20s%-10s%-10s%n", "name", "count", "sum");
        System.out.println("=".repeat(dash));

        // оголошуємо змінну для визначення топ-5
        int topFive = 0;

        for (BuyingDto buyingDto : report.getData()) {


            //щоб уникнути дублювання коду будемо виводити на друк лише топ-5 продуктів за кількістю проданого товару
            if (topFive == 5) {
                break;
            }
// якщо не вводити таке обмеження (if topFive == 5), то на друк виведеться увесь перелік по спадаючому списку

            String name = buyingDto.getName();
            int count = buyingDto.getCount();
            String cost = Math.round(buyingDto.getSum()) + " uah";

            System.out.printf("%-20s%-10s%-10s%n", name, count, cost);

            System.out.println("-".repeat(dash));

            topFive++;

        }

        System.out.println("=".repeat(dash));
        System.out.println("Total sum: " + report.getTotalSum() + " uah");


        TreeSet<Integer> treeSet = new TreeSet<>();

        for (BuyingDto buyingDto : report.getData()) {
            int count = buyingDto.getCount();
            treeSet.add(count);
        }


    }
}
