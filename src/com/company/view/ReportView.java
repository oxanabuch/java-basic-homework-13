package com.company.view;

import com.company.dto.BuyingDto;
import com.company.dto.ReportDto;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.TreeSet;

public class ReportView {

    public static final String DESCRIPTION = "REPORT DATA: ";

    public void printDetails(ReportDto report) {

        int dash = 50;

        System.out.println("=".repeat(dash));
        System.out.println(DESCRIPTION + report.getName());
        System.out.println("=".repeat(dash));

        System.out.printf("%-20s%-10s%-10s%n", "name", "count", "sum");
        System.out.println("=".repeat(dash));

        for (BuyingDto buyingDto : report.getData()) {

            String name = buyingDto.getName();
            int count = buyingDto.getCount();
            String cost = Math.round(buyingDto.getSum()) + " uah";

            System.out.printf("%-20s%-10s%-10s%n", name, count, cost);

            System.out.println("-".repeat(dash));

        }

        System.out.println("=".repeat(dash));
        System.out.println("Total sum: " + report.getTotalSum() + " uah");





        TreeSet<Integer> treeSet = new TreeSet<>();

        for (BuyingDto buyingDto : report.getData()) {
            int count = buyingDto.getCount();
            treeSet.add(count);
        }



    }
}
